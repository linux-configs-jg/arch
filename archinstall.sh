				##########################
				#     ARCH INSTALLER     #
				##########################

# This script aims to install all the necessary packages to create a clean, useable GUI 
# system. This should be run after a bare arch install after you have logged into your
# system as your user for the first time. NOT AS ROOT.

# Install yay for AUR packages
sudo git clone https://aur.archlinux.org/yay-git.git /opt/yay-git
sudo chown -R jonathan:jonathan /opt/yay-git
cd /opt/yay-git
makepkg -si --noconfirm
cd $HOME

# Install necessary packages
pacman -S xorg-server xorg-xinit xorg-xsetroot xorg-xrandr bspwm sxhkd feh alacritty firefox rofi neovim picom ttf-nerd-fonts-symbols bdf-unifont ttf-font-awesome --noconfirm

yay -S polybar

# Install dotfiles and configure bare git repository in $HOME
echo ".cfg" >> $HOME/.gitignore
git clone --bare https://gitlab.com/linux-configs-jg/dots.git $HOME/.cfg
alias cgit='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME config --local status.showUntrackedFiles no
rm $HOME/.bashrc $HOME/.bash_profile
/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME checkout

